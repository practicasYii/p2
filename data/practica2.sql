DROP DATABASE IF EXISTS practica2;
CREATE DATABASE IF NOT EXISTS practica2
CHARACTER SET latin1
COLLATE latin1_swedish_ci;

-- 
-- Disable foreign keys
-- 
-- /*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Set character set the client will use to send SQL statements to the server
--
SET NAMES 'utf8';
USE practica2;

--
-- Definition for table autores
--
CREATE TABLE IF NOT EXISTS noticias (
  id_no int(11) NOT NULL AUTO_INCREMENT,
  titulo_no varchar(255) DEFAULT NULL,
  texto_no varchar(255) DEFAULT NULL,
  foto_no varchar (255),
  PRIMARY KEY (id_no)
)
ENGINE = INNODB
AUTO_INCREMENT = 5
AVG_ROW_LENGTH = 4096
CHARACTER SET latin1
COLLATE latin1_swedish_ci;

CREATE TABLE IF NOT EXISTS articulos (
  id_ar int(11) NOT NULL AUTO_INCREMENT,
  titulo_ar varchar(255) DEFAULT NULL,
  textocorto_ar varchar(255) DEFAULT NULL,
  textolargo_ar varchar(255) DEFAULT NULL,
  foto_ar varchar (255),
  PRIMARY KEY (id_ar) 
)
ENGINE = INNODB
AUTO_INCREMENT = 5
AVG_ROW_LENGTH = 4096
CHARACTER SET latin1
COLLATE latin1_swedish_ci;

/*
-- Dumping data for tables entradas y fotos
--
INSERT INTO entradas VALUES
(1, 'Ejemplo de clase'),
(2, 'El Yii es facilisimo'),
(3, 'Mi mama me mima');

INSERT INTO fotos VALUES
(1, 'Izquierda','','alternativo1'),
(2, 'Centro','','alternativo2'),
(3, 'Derecha','','alternativo3');
  */