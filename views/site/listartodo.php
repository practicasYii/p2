<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="noticias-index">

    <h1>Noticias</h1>

</div>

    <?= GridView::widget([
        'dataProvider' => $dataProvidern,
        'columns' => [
            'id_no',
            'titulo_no',
            [
              'label'=>'otro',
               'value'=>function($data){
                return strlen($data->titulo_no);
              }  
            ],
            'texto_no',
            [
            'label'=>'Foto',
            'format'=>'raw',
            'value' => function($data){
                return Html::img("@web/imgs/$data->foto_no",['class'=>'img-responsive']); 
            }
        ],
            
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<div class="noticias-index">

    <h1>Articulos</h1>

</div>
        <?= GridView::widget([
        'dataProvider' => $dataProvidera,
        'columns' => [
            'id_ar',
            'titulo_ar',
            'textocorto_ar',
            'foto_ar',
            
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    
    
    
</div>

